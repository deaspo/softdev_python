from django import forms
from django.forms import ModelForm
from my_library.models import Book
# Very good use of a manage_books form to handle editing

class BookForm(ModelForm):

    class Meta:
        model = Book

        fields = ['title', 'author', 'publisher', 'isbn', 'edition']


class ManageBooks(ModelForm):

    class Meta:
        model = Book

        fields = ['title', 'author', 'publisher',
                  'isbn', 'check_out', 'edition']